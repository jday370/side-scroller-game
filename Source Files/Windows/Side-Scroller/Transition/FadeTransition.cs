﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace SideScroller.Screen
{
    class FadeTransition : Transition
    {
        private float fadeSpeed;
        private static TimeSpan defaultDelay, timeDelay;
        public static TimeSpan TimeDelay
        {
            get
            {
                return timeDelay;
            }
            set
            {
                defaultDelay = value;
                timeDelay = defaultDelay;
            }
        }
        private bool increase, stopUpdating;

        public override void StartTransition(TransitionSpeed speed = TransitionSpeed.Medium)
        {
            switch (speed)
            {
                case TransitionSpeed.Slow:
                    fadeSpeed = 0.5f;
                    break;
                case TransitionSpeed.Medium:
                    fadeSpeed = 1;
                    break;
                case TransitionSpeed.Fast:
                    fadeSpeed = 2;
                    break;

            }
            alpha = 0;
            increase = true;
            base.StartTransition(speed);
        }

        public override void LoadContent(SwitchListener switchMethod, Texture2D texture)
        {
            LoadContent(switchMethod, texture, new TimeSpan(0, 0, 1));
        }

        public void LoadContent(SwitchListener switchMethod, Texture2D texture, TimeSpan delay)
        {
            base.LoadContent(switchMethod, texture);
            increase = false;
            TimeDelay = delay;
            scale = Math.Max(Engine.Dimensions.X, Engine.Dimensions.Y); // temp :P
        }

        public override void Update(GameTime gameTime)
        {
            if (IsActive)
            {
                if (!stopUpdating)
                {
                    float alphaChange = fadeSpeed * (float)gameTime.ElapsedGameTime.TotalSeconds;
                    if (!increase)
                        alpha -= alphaChange;
                    else
                        alpha += alphaChange;
                    if (alpha < 0)
                        alpha = 0;
                    else if (alpha > 1)
                        alpha = 1;
                    if (!increase && alpha == 0)
                        IsActive = false;
                }
                if (alpha == 1)
                {
                    stopUpdating = true;
                    timeDelay -= gameTime.ElapsedGameTime;
                    if (timeDelay.TotalSeconds <= 0)
                    {
                        switchMethod();
                        increase = !increase;
                        TimeDelay = defaultDelay;
                        stopUpdating = false;
                    }
                }
            }
        }
    }
}
