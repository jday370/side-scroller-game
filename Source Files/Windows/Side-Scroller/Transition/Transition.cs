﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace SideScroller.Screen
{
    enum TransitionSpeed
    {
        Slow, Medium, Fast
    }
    class Transition
    {
        protected Texture2D texture;
        protected Rectangle sourceRectangle;
        protected float scale, alpha;

        public delegate void SwitchListener();
        protected SwitchListener switchMethod;

        private bool _isactive;
        public bool IsActive
        {
            get
            {
                return _isactive;
            }
            set
            {
                _isactive = Engine.RestrictControls = value;
            }
        }

        public virtual float Alpha { get { return alpha; } set { alpha = value; } }

        public virtual void StartTransition(TransitionSpeed speed = TransitionSpeed.Medium)
        {
            IsActive = true;
        }

        public virtual void LoadContent(SwitchListener switchMethod, Texture2D texture)
        {
            this.switchMethod = switchMethod;
            this.texture = texture;
            if (texture != null)
                sourceRectangle = new Rectangle(0, 0, texture.Width, texture.Height);
            scale = alpha = 1;
            IsActive = false;
        }

        public void UnloadContent()
        {
            texture = null;
            sourceRectangle = Rectangle.Empty;
        }

        public virtual void Update(GameTime gameTime)
        {

        }

        public void Draw(Graphics g)
        {
            if (IsActive && texture != null)
            {
                g.Draw(texture, Vector2.Zero, sourceRectangle, Color.White * alpha,
                    0, Vector2.Zero, scale, SpriteEffects.None, 0);
            }
        }

    }
}
