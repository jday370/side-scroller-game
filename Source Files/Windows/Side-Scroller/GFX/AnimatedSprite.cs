﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace SideScroller.GFX
{
    class AnimatedSprite : Sprite
    {
        public int Rows { get; set; }
        public int Columns { get; set; }
        public int CurrentFrame { get; set; }
        public int TotalFrames { get; set; }
        public override int ImgWidth { get { return Texture.Width / Columns; } }
        public override int ImgHeight { get { return Texture.Height / Rows; } }
        //public override int Width { get { return ImgWidth; } }
        //public override int Height { get { return ImgHeight; } } 

        private Util.BufferedUpdate buffer;

        public AnimatedSprite(Texture2D texture, int rows, int columns, double fps)
            : base(texture)
        {
            Rows = rows;
            Columns = columns;
            CurrentFrame = 0;
            TotalFrames = Rows * Columns;
            buffer = new Util.BufferedUpdate(fps);

            int row = (int)((float)CurrentFrame / (float)Columns);
            int column = CurrentFrame % Columns;
            Rectangle rectangle = new Rectangle(ImgWidth * column, ImgHeight * row, ImgWidth, ImgHeight);
            Color[] colorData = Pixels;
            Pixels = new Color[rectangle.Width * rectangle.Height];
            for (int x = 0; x < rectangle.Width; x++)
                for (int y = 0; y < rectangle.Height; y++)
                    Pixels[x + y * rectangle.Width] = colorData[x + rectangle.X + (y + rectangle.Y) * Texture.Width];
        }

        public override void Update(GameTime gameTime)
        {
            buffer.Update(gameTime, delegate()
            {
               CurrentFrame = CurrentFrame + 1 == TotalFrames ? 0 : CurrentFrame + 1;
            });
        }

        public override void Draw(Graphics g, Vector2 location)
        {
            int row = (int)((float)CurrentFrame / (float)Columns);
            int column = CurrentFrame % Columns;

            Rectangle sourceRectangle = new Rectangle(ImgWidth * column, ImgHeight * row, ImgWidth, ImgHeight);

            g.Draw(Texture, location, sourceRectangle, Color.White);
        }

    }
}
