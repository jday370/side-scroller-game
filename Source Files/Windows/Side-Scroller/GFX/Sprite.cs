﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace SideScroller.GFX
{
    class Sprite
    {
        protected Texture2D Texture { get; set; }
        public Color[] Pixels { get; set; }
        public virtual int ImgWidth { get { return Texture.Width; } }
        public virtual int ImgHeight { get { return Texture.Height; } }
        public virtual int Width { get { return ImgWidth; } }
        public virtual int Height { get { return ImgHeight; } }

        public Sprite(Texture2D texture)
        {
            Texture = texture;
            Pixels = new Color[Texture.Width * Texture.Height];
            Texture.GetData<Color>(Pixels);
        }

        public virtual void Update(GameTime gameTime) { }

        public virtual void Draw(Graphics g, Vector2 location)
        {
            g.Draw(Texture, location, Color.White);
        }

    }
}
