﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using SideScroller.Entity;
using SideScroller.GFX;

namespace SideScroller.Screen
{
    class GameScreen : Screen
    {
        private Texture2D background;

        private Player player;
        public static Platforms platforms;

        private SpriteFont font;

     ///\\   private ContentManager levelContent; //\\\

        public override void LoadContent()
        {
            Input.Reset();
            Input.Pressed += new Input.KeyEventHandler(KeyPressed);
            Input.Released += new Input.KeyEventHandler(KeyReleased);

            font = Util.LoadFont(AllResources.ResourceManager, "GameFont");

            background = Util.LoadImage(AllResources.ResourceManager, "background");
            platforms = new Platforms(background, new Rectangle(0, 0, 800, 600));

            AnimatedSprite shipSprite = new AnimatedSprite(Util.LoadImage(AllResources.ResourceManager, "char1"), 4, 4, 24);
            player = new Player(shipSprite, new Vector2(20, platforms.GetYFor(20) - shipSprite.Height));
        }

        public override void UnloadContent()
        {
            base.UnloadContent();
        }

        public override void Update(Microsoft.Xna.Framework.GameTime gameTime)
        {
            player.Update(gameTime);
        }

        private void KeyPressed(Keys key)
        {

        }

        private void KeyReleased(Keys key)
        {

        }

        public override void Draw(Graphics g)
        {
            g.Draw(background, new Rectangle(0, 0, (int)Engine.Dimensions.X, (int)Engine.Dimensions.Y), Color.White);
            player.Draw(g);
        }
    }
}
