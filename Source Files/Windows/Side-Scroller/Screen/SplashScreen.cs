﻿using System;
using System.Threading;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using SideScroller.IO;

namespace SideScroller.Screen
{
    class SplashScreen : Screen
    {
        private Thread loadingThread;

        private SpriteFont font;
        private FadeTransition transition;
        private Texture2D[] images = new Texture2D[2+2]; // add two for filler images
        private int currentImage;

        private TimeSpan defaultTime, sleepTime;

        public override void LoadContent()
        {
            font = Util.LoadFont(AllResources.ResourceManager, "GameFont");

            transition = new FadeTransition();
            transition.LoadContent(IncrementImage, Util.BlackTexture);

            defaultTime = new TimeSpan(0, 0, 3); // how much time waiting while image is shown fully opaque
            sleepTime = defaultTime;

            currentImage = 0;

            for (int i = 1; i < images.Length-1; i++)
            {
                images[i] = Util.LoadImage(AllResources.ResourceManager, "splash" + i);
                Util.ChangeColor(images[i], Color.White, Color.Transparent); // temp
            }

            Input.Reset();
            Input.Pressed += new Input.KeyEventHandler(delegate(Keys key) { NextImage(); });

            loadingThread = new Thread(LoadGameContent);
            loadingThread.Name = "Loading Thread";
            loadingThread.Start();

            transition.StartTransition();
        }

        private void NextScreen()
        {
            loadingThread.Join();
            ScreenManager.Instance.AddScreen(new TitleScreen(), true, TransitionSpeed.Fast);
        }

        private void NextImage()
        {
            if (currentImage + 1 == images.Length) NextScreen();
            else
            {
                transition.LoadContent(IncrementImage, Util.BlackTexture, new TimeSpan(0, 0, 3));
                transition.StartTransition(TransitionSpeed.Slow);
            }
        }

        private void IncrementImage()
        {
            currentImage++;
        }

        public override void Update(GameTime gameTime)
        {
            if (!transition.IsActive)
            {
                if (currentImage == images.Length - 1) NextScreen();
                sleepTime -= gameTime.ElapsedGameTime;
                if (sleepTime.TotalSeconds <= 0)
                {
                    sleepTime = defaultTime;
                    NextImage();
                }
            }
            else
                transition.Update(gameTime);
        }

        private void LoadGameContent()
        {
            // Load Stuff while the splash screen is doing stuff
        }

        public override void UnloadContent()
        {
            base.UnloadContent();
        }

        public override void Draw(Graphics g)
        {
            if (currentImage == images.Length - 1)
            {
                string loading = "Loading...";
                g.DrawString(Engine.Font, loading,
                    new Vector2(Engine.Dimensions.X - Engine.Font.MeasureString(loading).X - 5, Engine.Dimensions.Y - Engine.Font.MeasureString(loading).Y - 5), Color.White);
            }
            else if (currentImage != 0)
            {
                g.Draw(images[currentImage], new Rectangle(0, 0, (int)Engine.Dimensions.X, (int)Engine.Dimensions.Y), Color.White);
            }
            transition.Draw(g);
        }
    }
}
