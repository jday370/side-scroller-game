﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace SideScroller.Screen
{
    class ScreenManager
    {
        private static ScreenManager instance;
        public static ScreenManager Instance
        {
            get
            {
                if (instance == null) instance = new ScreenManager();
                return instance;
            }
        }

        Stack<Screen> screenStack = new Stack<Screen>();

        private Screen currentScreen { get { return screenStack.Count == 0 ? null : screenStack.Peek(); } }

        private Screen addScreen;
        private bool popAddScreen;

        private FadeTransition transition;

        public void AddScreen(Screen screen, bool removeCurrentScreen = false, TransitionSpeed speed = TransitionSpeed.Medium)
        {
            addScreen = screen;
            popAddScreen = removeCurrentScreen;
            transition.StartTransition(speed);
        }

        public void PopScreen()
        {
            transition.StartTransition();
        }

        public void Init()
        {
            screenStack.Push(new SplashScreen()); // should be new SplashScreen();
            transition = new FadeTransition();
        }

        public void LoadContent()
        {
            currentScreen.LoadContent();

            transition.LoadContent(TransitionComplete, Util.BlackTexture);
        }

        public void Update(GameTime gameTime)
        {
            if (!transition.IsActive)
                currentScreen.Update(gameTime);
            else
                transition.Update(gameTime);
        }

        public void Draw(Graphics g)
        {
            currentScreen.Draw(g);
            transition.Draw(g);
        }

        private void TransitionComplete()
        {
            if (addScreen != null)
            {
                currentScreen.UnloadContent();
                if (popAddScreen) screenStack.Pop();
                addScreen.LoadContent();
                screenStack.Push(addScreen);
                addScreen = null;
            }
            else
            {
                screenStack.Pop().UnloadContent();
                currentScreen.LoadContent();
            }
        }
    }
}
