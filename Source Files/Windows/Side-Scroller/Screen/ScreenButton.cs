﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace SideScroller.Screen
{
    class ScreenButton
    {
        public static readonly float DefaultAlpha = 0.3f;

        public string Text { get; set; }
        private Vector2 position;
        private float alpha;
        private bool increase;

        public float Alpha
        {
            get
            {
                return alpha;
            }
            set
            {
                alpha = value;
                if (alpha == 1) increase = false;
                else increase = true;
            }
        }

        public bool ShowArrow { get; set; }

        public ScreenButton(string text, Vector2 position)
        {
            Text = text;
            this.position = position;
            Alpha = DefaultAlpha;
        }

        public void Update(GameTime gameTime)
        {
            float alphaChange = (float)gameTime.ElapsedGameTime.TotalSeconds;
            if (increase) alpha += alphaChange;
            else alpha -= alphaChange;
            if (alpha >= 1)
            {
                alpha = 1f;
                increase = false;
            }
            else if (alpha <= DefaultAlpha)
            {
                alpha = DefaultAlpha;
                increase = true;
            }
        }

        public void Draw(Graphics g)
        {
            if (ShowArrow)
            {
                g.DrawLine(new Vector2(position.X - 10, position.Y + 10),
                    new Vector2(position.X - 5, position.Y + Engine.Font.MeasureString(Text).Y / 2), Color.White);
                g.DrawLine(new Vector2(position.X - 10, position.Y + Engine.Font.MeasureString(Text).Y - 10),
                    new Vector2(position.X - 5, position.Y + Engine.Font.MeasureString(Text).Y / 2), Color.White);
            }
            g.DrawString(Engine.Font, Text, position, Color.White * alpha);
        }
    }
}
