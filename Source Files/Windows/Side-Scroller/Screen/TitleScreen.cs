﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace SideScroller.Screen
{
    class TitleScreen : Screen
    {
        private string[] menuItems = {
            "Online", "Offline", "Options", "Credits"
        };

        private Texture2D menuImage;

        private Stack<ScreenButton[]> buttonStack;
        private ScreenButton[] menuButtons
        {
            get
            {
                return buttonStack.Count == 0 ? null : buttonStack.Peek();
            }
        }

        private int _selected = 0;
        private int Selected
        {
            get
            {
                return _selected;
            }
            set
            {
                menuButtons[Selected].Alpha = ScreenButton.DefaultAlpha;
                menuButtons[Selected].ShowArrow = false;
                if (value == -1) _selected = menuButtons.Length - 1;
                else if (value == menuButtons.Length) _selected = 0;
                else _selected = value;
               menuButtons[Selected].ShowArrow = true;
            }
        }

        public override void LoadContent()
        {
            buttonStack = new Stack<ScreenButton[]>();

            menuImage = null; // TODO: load

            ReloadButtons();
            menuButtons[Selected].ShowArrow = true;

            Input.Reset();
            Input.Pressed += new Input.KeyEventHandler(KeyPressed);
        }

        private void ReloadButtons()
        {
            ScreenButton[] buttons = new ScreenButton[menuItems.Length];
            for (int i = 0; i < buttons.Length; i++)
            {
                buttons[i] = new ScreenButton(menuItems[i],
                    new Vector2(Engine.Dimensions.X / 2 - Engine.Font.MeasureString(menuItems[i]).X / 2,
                        Engine.Dimensions.Y / 2 - (menuItems.Length / 6) * 100 + i * 50));
            }
            buttonStack.Push(buttons);
            Selected = 0;
        }

        public override void UnloadContent()
        {
            base.UnloadContent();
        }

        public override void Update(GameTime gameTime)
        {
            menuButtons[Selected].Update(gameTime);
        }

        private void KeyPressed(Keys key)
        {
            switch (key)
            {
                case Keys.W:
                case Keys.Up:
                    Selected--;
                    break;
                case Keys.S:
                case Keys.Down:
                    Selected++;
                    break;
                case Keys.A:
                case Keys.Left:
                case Keys.Delete:
                    if (buttonStack.Count > 1)
                    {
                        buttonStack.Pop();
                        Selected = 0;
                    }
                    break;
                case Keys.Enter:
                case Keys.Space:
                case Keys.D:
                case Keys.Right:
                    switch (menuButtons[Selected].Text)
                    {
                        case "Online":
                            menuItems = new string[] {
                                "Login", "New User"
                            };
                            ReloadButtons();
                            break;
                        case "Offline":
                            ScreenManager.Instance.AddScreen(new GameScreen(), false, TransitionSpeed.Slow);
                            break;
                        case "Options":
                            // TODO: options
                            break;
                        case "Credits":
                            // TODO: credits
                            break;
                    }
                    break;
            }
        }

        public override void Draw(Graphics g)
        {
            g.FillRect(new Rectangle(30, 30, (int)Engine.Dimensions.X - 60, 200), Color.DarkSlateGray);
            foreach (ScreenButton button in menuButtons)
                button.Draw(g);
        }
    }
}
