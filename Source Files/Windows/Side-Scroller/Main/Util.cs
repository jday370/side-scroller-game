﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using System.Resources;

namespace SideScroller
{
    class Util
    {
        #region Utility Fields
        public static Texture2D BlackTexture;

        static Util()
        {
            BlackTexture = new Texture2D(Engine.Instance.GraphicsDevice, 1, 1);
            BlackTexture.SetData<Color>(new Color[] { Color.Black });
        }
        #endregion

        #region Pixel Modification Methods
        public static void ChangeColor(Texture2D texture, Color color, Color newColor)
        {
            Color[] pixels = new Color[texture.Width * texture.Height];
            texture.GetData<Color>(pixels);

            for (int i = 0; i < pixels.Length; i++)
            {
                if (pixels[i] == color)
                    pixels[i] = newColor;
            }

            texture.SetData<Color>(pixels);
        }
        #endregion

        #region Collision Detection Methods
        public static bool CollidesWith(Entity.Entity a, Entity.Entity b)
        {
            return CollidesWith(a, b, true);
        }
        public static bool CollidesWith(Entity.Entity a, Entity.Entity b, bool calcPerPixel)
        {
            if (calcPerPixel &&
                ((Math.Min(a.Width, b.Height) > 3) ||
                (Math.Min(a.Width, b.Height) > 3))) // change? for small entities, don't do per-pixel
            {
                return Intersects(a, b)
                    && PerPixelCollision(a, b);
            }
            return Intersects(a, b);
        }
        private static bool PerPixelCollision(Entity.Entity a, Entity.Entity b)
        {
            int x1 = Math.Max((int)a.X, (int)b.X);
            int x2 = Math.Min((int)a.X + a.Width, (int)b.X + b.Width);

            int y1 = Math.Max((int)a.Y, (int)b.Y);
            int y2 = Math.Min((int)a.Y + a.Height, (int)b.Y + b.Height);

            for (int y = y1; y < y2; ++y)
            {
                for (int x = x1; x < x2; ++x)
                {
                    Color colA = a.Pixels[(x - (int)a.X) + (y - (int)a.Y) * a.Width];
                    Color colB = b.Pixels[(x - (int)b.X) + (y - (int)b.Y) * b.Width];

                    if (colA.A != 0 && colB.A != 0)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        private static bool Intersects(Entity.Entity ent1, Entity.Entity ent2)
        {
            //    return left1 < right2 && right1 > left2 && up1 < down2 && down1 > up2;
            return ent1.X < ent2.X + ent2.Width && ent1.X + ent1.Width > ent2.X && ent1.Y < ent2.Y + ent2.Height && ent1.Y + ent1.Height > ent2.Y;
        }
        #endregion

        #region Random Methods
        private static readonly Random random = new Random();

        public static int Random(int minValue, int maxValue)
        {
            if (maxValue <= minValue) throw new FormatException("Incorrect format: maxValue must be more than minValue.");
            return random.Next(minValue, maxValue);
        }

        public static double Random(double minValue, double maxValue)
        {
            if (maxValue <= minValue) throw new FormatException("Incorrect format: maxValue must be more than minValue.");
            return minValue + random.NextDouble() * maxValue;
        }
        #endregion

        #region Buffered Update
        public class BufferedUpdate
        {
            public delegate void UpdateMethod();
            private double timePassed, fps;

            public BufferedUpdate(double fps)
            {
                this.fps = fps;
            }

            public void Update(GameTime gameTime, UpdateMethod update)
            {
                timePassed += gameTime.ElapsedGameTime.TotalSeconds;
                while (timePassed >= (1 / fps))
                {
                    update();
                    timePassed -= (1 / fps);
                }
            }
        }
        #endregion

        #region Methods for Loading Content From XNB Files
        private static List<ContentManager> contentManagers = new List<ContentManager>();

        public static Texture2D LoadImage(ResourceManager resource, string name)
        {
            Texture2D texture = null;
            foreach (ContentManager content in contentManagers)
                if (content.ServiceProvider == resource)
                {
                    texture = content.Load<Texture2D>(name);
                    break;
                }
            if (texture == null)
            {
                ContentManager content = new ResourceContentManager(Engine.Instance.Services, resource);
                texture = content.Load<Texture2D>(name);
                contentManagers.Add(content);
            }
            return texture;
        }

        public static SpriteFont LoadFont(ResourceManager resource, string name)
        {
            SpriteFont font = null;
            foreach (ContentManager content in contentManagers)
                if (content.ServiceProvider == resource)
                {
                    font = content.Load<SpriteFont>(name);
                    break;
                }
            if (font == null)
            {
                ContentManager content = new ResourceContentManager(Engine.Instance.Services, resource);
                font = content.Load<SpriteFont>(name);
                contentManagers.Add(content);
            }
            return font;
        }

        public static void Unload(ResourceManager resource)
        {
            ContentManager content = GetContent(resource, false);
            if (content != null) content.Unload();
        }

        private static ContentManager GetContent(ResourceManager resource, bool createNew = true)
        {
            foreach (ContentManager content in contentManagers)
                if (content.ServiceProvider == resource) return content;
            if (createNew)
            {
                ContentManager newContent = new ResourceContentManager(Engine.Instance.Services, resource);
                contentManagers.Add(newContent);
                return newContent;
            }
            return null;
        }
        #endregion
    }
}
