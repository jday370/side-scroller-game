﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;

using SideScroller.Entity;
using SideScroller.GFX;
using SideScroller.Screen;
using System.Resources;

namespace SideScroller
{
    class Engine : Game
    {
        public static readonly string Title = "Side-Scroller Demo Pre-alpha, v1.0.0";

        private static Engine instance;
        public static Engine Instance { get { return instance; } }

        private GraphicsDeviceManager graphics;
        private Graphics g;

        private readonly Vector2 aspectRatio = new Vector2(16, 9);
        private static Vector2 dimensions;
        public static Vector2 Dimensions { get { return dimensions; } }

        public static SpriteFont Font { get; set; }

        public static bool RestrictControls { get; set; }

        public Engine()
            : base()
        {
            instance = this;
            graphics = new GraphicsDeviceManager(this);

            IsFixedTimeStep = false;
            IsMouseVisible = true;
            Window.Title = Title;
        }

        protected override void Initialize()
        {
            ScreenManager.Instance.Init();
            graphics.PreferredBackBufferWidth = (int)aspectRatio.X * 60; // remove *60 for release
            graphics.PreferredBackBufferHeight = (int)aspectRatio.Y * 60;
            //    graphics.IsFullScreen = true;
            graphics.ApplyChanges();
            dimensions = new Vector2(Window.ClientBounds.Width, Window.ClientBounds.Height);
            base.Initialize();
        }

        protected override void LoadContent()
        {
            g = new Graphics(GraphicsDevice);
            Font = Util.LoadFont(AllResources.ResourceManager, "GameFont");

            ScreenManager.Instance.LoadContent();
        }

        protected override void UnloadContent() { }

        private KeyboardState prevKeyState;
        protected override void Update(GameTime gameTime)
        {
            #region Input Handling
            KeyboardState keyState = Keyboard.GetState();

            if (keyState.IsKeyDown(Keys.Escape))
                Exit();

            for (int i = 0; i < 254; i++)
            {
                if (prevKeyState.IsKeyUp((Keys)i) && keyState.IsKeyDown((Keys)i))
                    Input.OnPressed((Keys)i);
                else if (prevKeyState.IsKeyDown((Keys)i) && keyState.IsKeyUp((Keys)i))
                    Input.OnReleased((Keys)i);
            }

            prevKeyState = keyState;
            #endregion

            ScreenManager.Instance.Update(gameTime);

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            g.Begin();
            ScreenManager.Instance.Draw(g);
            g.End();

            base.Draw(gameTime);
        }
    }
}
