﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;

namespace SideScroller
{
    class Input
    {
        public delegate void KeyEventHandler(Keys key);
        public static event KeyEventHandler Pressed, Released;

        public static void Reset()
        {
            Pressed = null;
            Released = null;
        }

        public static void OnPressed(Keys key)
        {
            if (!Engine.RestrictControls && Pressed != null)
                Pressed(key);
        }

        public static void OnReleased(Keys key)
        {
            if (!Engine.RestrictControls && Released != null)
                Released(key);
        }
        
    }
}
