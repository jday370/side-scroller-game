﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using System.Collections;

namespace SideScroller.IO
{
    // Class in development
    class FileManager
    {
        public static List<string> Load(string fileName, string element)
        {
            if (!File.Exists(fileName)) return CreateDefaultFile(fileName, element);

            List<string> vals = new List<string>();

            //XDocument doc = XDocument.Load(fileName);
            //IEnumerable elements = doc.Descendants(element);

            //foreach (XElement e in elements) {
            //    vals.Add(e.Value);
            //}

            using (var reader = new XmlTextReader(fileName))
            {
                while (!reader.Read())
                {
                    if (reader.IsStartElement() && reader.Name == element)
                    {
                        reader.Read();
                        vals.Add(reader.Value);
                    }
                }
            }
            return vals;
        }

        private static List<string> CreateDefaultFile(string fileName, string element)
        {
            List<string> vals = new List<string>();

            string[] elements = new string[] {
                "Element1",
                    "Element2",
                        "FirstName(Element)=Bob",
                 "//"
            };

            #region Handle Writing
            using (var writer = XmlWriter.Create(fileName))
            {
                writer.WriteStartDocument();
                foreach (string e in elements)
                {
                    int numOfEnd = NumOfAtFront(e, '/');
                    if (numOfEnd != 0)
                    {
                        for (int i = 0; i < numOfEnd; i++)
                            writer.WriteEndElement();
                        if (String.IsNullOrWhiteSpace(e.Substring(e.LastIndexOf('/'))))
                        {
                            continue;
                        }
                    }
                    if (e.Contains('='))
                    {
                        string type = e.Substring(e.IndexOf('(') + 1, e.IndexOf(')'));
                        string property = e.Substring(0, e.IndexOf('('));
                        string value = e.Substring(e.IndexOf('=') + 1);
                        if (type == "Element")
                        {
                            writer.WriteElementString(property, value);
                            if (e == element) vals.Add(value);
                        }
                        else if (type == "Attribute")
                        {
                            writer.WriteAttributeString(property, value);
                        }
                    }
                    else
                    {
                        writer.WriteStartElement(e);
                    }

                }
                writer.WriteEndDocument();
            }
            #endregion

            return vals;
        }

        private static int NumOfAtFront(string s, char c)
        {
            int num = 0;
            while (s.IndexOf('/') == 0)
            {
                num++;
                s = s.Substring(1);
            }
            return num;
        }
    }
}
