﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using SideScroller.GFX;
using SideScroller.Screen;

namespace SideScroller.Entity
{
    class Player : Entity
    {
        public bool up, down, left, right, jumping;
        private float yVelocity;
        private new AnimatedSprite sprite;

        public Player(Sprite sprite, Vector2 location)
            : base(sprite, location)
        {
            this.sprite = (AnimatedSprite)sprite;
            Input.Pressed += new Input.KeyEventHandler(KeyPressed);
            Input.Released += new Input.KeyEventHandler(KeyReleased);
        }

        private void KeyPressed(Keys key)
        {
            toggle(key, true);
        }

        private void KeyReleased(Keys key)
        {
            toggle(key, false);
        }

        private void toggle(Keys key, bool b)
        {
            switch (key)
            {
                case Keys.Up:
                case Keys.W:
                    up = b;
                    break;
                case Keys.Down:
                case Keys.S:
                    down = b;
                    break;
                case Keys.Left:
                case Keys.A:
                    left = b;
                    break;
                case Keys.Right:
                case Keys.D:
                    right = b;
                    break;
            }
        }

        private Util.BufferedUpdate buffer = new Util.BufferedUpdate(60);
        public override void Update(GameTime gameTime)
        {
            buffer.Update(gameTime, delegate()
            {
                bool onPlatform = IsOnPlatform();
                if (!up && !onPlatform && yVelocity == 0)
                {
                    jumping = true;
                    yVelocity = -0.3f;
                }
                if (isMoving())
                {
                    sprite.Update(gameTime);
                    if (left)
                        X-=4;
                    if (right)
                        X += 4;
                    Y = GameScreen.platforms.GetYFor((int)X);
                }
                if (up && !jumping)
                {
                    yVelocity = -7f;
                    jumping = true;
                }
                if (down)
                    ; // TODO: have it make char crouch (this changes width & height of char)

                if (jumping)
                {
                    yVelocity += 0.2f; // Gravity = 0.4f*60 = 24 pixels per second
                    Y += yVelocity;
                    if (onPlatform)
                    {
                        jumping = false;
                        Y = GameScreen.platforms.GetYFor((int)X);
                        yVelocity = 0;
                    }
                }
            });
        }

        public bool isMoving() { return left && !right || right && !left; }
        private bool IsOnPlatform()
        {
            return GameScreen.platforms.CollidesWithBottom(this);
        }
    }
}
