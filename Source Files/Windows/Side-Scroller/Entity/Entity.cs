﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using SideScroller.GFX;

namespace SideScroller.Entity
{
    abstract class Entity
    {
        protected Sprite sprite;
        protected Vector2 Location;
        public float X { get { return Location.X; } set { Location.X = value; } }
        public float Y { get { return Location.Y; } set { Location.Y = value; } }
        public int Width { get { return sprite.Width; } }//set { sprite.scale.X = (Width*2-value)/Width; } }
        public int Height { get { return sprite.Height; } }// set { sprite.scale.Y = (Height*2 - value) / Height; } }
        public Color[] Pixels { get { return sprite.Pixels; } }

        public Entity(Sprite sprite, Vector2 location)
        {
            this.sprite = sprite;
            this.Location = location;
        }

        public virtual void Update(GameTime gameTime) { }

        public void Draw(Graphics g)
        {
            sprite.Draw(g, Location);
        }
    }
}
