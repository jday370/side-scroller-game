﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using SideScroller.GFX;
using Microsoft.Xna.Framework.Graphics;

namespace SideScroller.Entity
{
    class Platforms
    {
        private Texture2D texture;
        private Rectangle bounds;
        public List<Color> Pixels { get; set; }

        public Platforms(Texture2D texture, Rectangle bounds) {
            this.texture = texture;
            this.bounds = bounds;
            Pixels = new List<Color>();
            Color[] data = new Color[texture.Width * texture.Height];
            texture.GetData<Color>(data);
            for (int i = 0; i < data.Length; i++)
            {
                if (data[i] != Color.Yellow && data[i] != Color.Red) continue;
                Pixels.Add(data[i]);
            }
        }

        public bool CollidesWithBottom(Entity ent)
        {
            return false;
        }
        public static bool CollidesWith(Entity a, Entity b, bool calcPerPixel)
        {
              return PerPixelCollision(a, b);
        }
        private static bool PerPixelCollision(Entity a, Entity b)
        {
            int x1 = Math.Max((int)a.X, (int)b.X);
            int x2 = Math.Min((int)a.X + a.Width, (int)b.X + b.Width);

            int y1 = Math.Max((int)a.Y, (int)b.Y);
            int y2 = Math.Min((int)a.Y + a.Height, (int)b.Y + b.Height);

            for (int y = y1; y < y2; ++y)
            {
                for (int x = x1; x < x2; ++x)
                {
                    Color colA = a.Pixels[(x - (int)a.X) + (y - (int)a.Y) * a.Width];
                    Color colB = b.Pixels[(x - (int)b.X) + (y - (int)b.Y) * b.Width];

                    if (colA.A != 0 && colB.A != 0)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        private static bool Intersects(Entity ent1, Entity ent2)
        {
            //    return left1 < right2 && right1 > left2 && up1 < down2 && down1 > up2;
            return ent1.X < ent2.X + ent2.Width && ent1.X + ent1.Width > ent2.X && ent1.Y < ent2.Y + ent2.Height && ent1.Y + ent1.Height > ent2.Y;
        }

        public int GetYFor(int x)
        {
            return 0;
        }
    }
}